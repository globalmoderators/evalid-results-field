<?php

namespace Globalmoderators\EvalidResultsField;

use Laravel\Nova\Fields\Field;

class EvalidResultsField extends Field
{
    /**
     * The field's component.
     *
     * @var string
     */
    public $component = 'evalid-results-field';

    public function __construct($name, $attribute = null, callable $resolveCallback = null)
    {
        parent::__construct($name, $attribute, $resolveCallback);
        $this->showOnDetail = true;
        $this->showOnIndex = false;
        $this->showOnCreation = false;
        $this->showOnUpdate = false;
    }
}
